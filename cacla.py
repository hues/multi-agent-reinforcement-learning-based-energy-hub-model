# -*- coding: utf-8 -*-
"""
Created on Tue Mar  1 09:13:43 2016

@author: boa
"""

"""
Implementation of the Continuous actor critic automaton. Uses neural networks
as actor and critic.
"""

from pymlp.mlp.NeuralNetworkAdapter import FFNetworkFactory
import numpy as np


class Cacla():
    """
    Implementation of Continuous Actor-Critic Learning Automata (CACLA).
    """

    def __init__(self, actorLayout, transferFunctions, replicationNumber, 
                 alpha=0.2, beta=0.2, gamma=0.1, sigma=0.05,
                 folder="Cacla"):
        """
        Initializes the basic member variables, not the controller itself.

        Parameters:
            actorLayout -- the layout of the actor's neural network
            transferFunctions -- the transfer functions for the neural network
            replicationNumber -- the number for the replication, simply an
                                 identifier for the controllers
            maxReward -- the maximum possible reward so the received reward can
                         be scaled to be < 1
            alpha -- the critic's learning rate
            beta -- the actor's learning rate
            gamma -- the discount factor
            sigma -- the standard deviation for the exploration
            folder -- the folder in which the data shall be stored
       """      
        # Make sure it uses its own random seed
        self.randomState = np.random.RandomState()
        self.zeta = 0.001

        self.inputDimensions = actorLayout[0] 
        self.actionDimensions = actorLayout[-1] #last element is the dimensions of the action space
        self.actorLayout = actorLayout
        self.transferFunctions = transferFunctions

        #self.maxReward = maxReward
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma
        self.sigma = sigma
        self.replicationNumber = replicationNumber
        self.folder = folder
        self.networkFactory = FFNetworkFactory(folder)
        self.errors = list()
        self.actorErrors = list()
        self.sigmas = list()

    def initController(self):
        """
        Initializes the controllers for the critic and actor.
        """                
        #creates the neural network for the critic and actor
        #these are paramaterized function approximators, which are used
        #to store the value of observed states and generalize to unseen states
        self.critic = self.networkFactory.createNetwork("critic",
                self.actorLayout[0:-1] + (1,), self.transferFunctions,
                self.alpha, self.replicationNumber)   

        self.actor = self.networkFactory.createNetwork("actor",
                self.actorLayout, self.transferFunctions, self.beta,
                self.replicationNumber)
        self.var = 1.0 

    def updateReward(self, state, reward, nextState, action, episodeOver, agent):
        """
        Updated the reward according to the algorithms definition.

        Parameters:
            state -- the current state of the environment
            reward -- the just received reward
            nextState -- the state of the environment after performing the
                         action
            action -- the just performed action
            episodeOver -- True if this was the last step in this episode
        """
        
        #this is now unnecessary b/c the reward is already scaled prior to this
        #reward = self.scaleReward(reward, maxReward)
        futureOpinion = self.getCriticOpinion(nextState)
                
        
        # ignore the future, if there is none, because this episode is over
        stillRunning = not episodeOver
        
        target = reward + (stillRunning * self.gamma * futureOpinion)

        criticOpinion = self.getCriticOpinion(state)  
        
        delta_t = (target - criticOpinion)
        self.errors.append(delta_t)
        
        #changed this from below. might be a stupid fix. watch out.
        #numIt = np.nan_to_num(delta_t / np.sqrt(self.var))
        #numIt = numIt[0]
        #numIt = numIt[0]
        #numIt = int(np.ceil(numIt))
        numIt = np.ceil(np.nan_to_num(delta_t / np.sqrt(self.var)))
        numIt = int(numIt)
        
        self.var = self.updateVar(delta_t)
        self.critic.train(np.array([state]), np.array([target]), 1, agent)
        updateActor = delta_t[0] > 0  # actual performance better than expected
        if updateActor:
            self.actor.train(np.array([state]), np.array([action]), numIt, agent)
            
        #if agent.technology_name == 'battery':
            #print('state:', state, 'reward:', reward, 'critic opinion:', criticOpinion, 'delta_t:', delta_t)

    def updateRewardX(self, state, reward, nextState, action, episodeOver, agent):
        """
        Updated the reward according to the algorithms definition.

        Parameters:
            state -- the current state of the environment
            reward -- the just received reward
            nextState -- the state of the environment after performing the
                         action
            action -- the just performed action
            episodeOver -- True if this was the last step in this episode
        """

        #reward = self.scaleReward(reward)

        futureOpinion = self.getCriticOpinion(np.concatenate((nextState, self.getAction(nextState, agent))))
        # ignore the future, if there is none, because this episode is over
        stillRunning = not episodeOver
        target = reward + (stillRunning * self.gamma * futureOpinion)

        criticOpinion = self.getCriticOpinion(np.concatenate((state, action)))
        delta_t = (target - criticOpinion)
        self.errors.append(delta_t)
        numIt = np.ceil(np.nan_to_num(delta_t / np.sqrt(self.var)))
        numIt = int(numIt)
        self.var = self.updateVar(delta_t)
        self.critic.train(np.array([np.concatenate((state, action))]), np.array([target]), 1, agent)
        updateActor = delta_t[0] > 0  # actual performance better than expected
        if updateActor:
            self.actor.train(np.array([state]), np.array([action]), numIt, agent)    
    
    
    def updateVar(self, delta_t):
        """
        Updated the running variance based on the td-error.

        Parameters:
            delta_t -- the current temporal difference error
            returns -- the new value for the running variance
        """
        return (1 - self.zeta) * self.var + self.zeta * delta_t ** 2

    def _explore(self, actions):
        """
        Adds the exploration to the actions. Uses a simple simulation of the
        truncated normal distribution.

        Parameters:
            actions -- actions to which the exploratory noise shall be added
            returns -- the action + noise
        """

        explored = actions
        if self.sigma >0:
            explored = map(self._sample, actions)
            explored = list(explored)

        return np.array(explored)

    def _sample(self, mean):
        """
        Samples from a truncated normal distribution with the borders -1, 1
        around the given mean value.

        Parameters:
            mean -- the mean value of the truncated normal distribution to
                    sample from
        """
        self.randomState = np.random.RandomState()
        X = self.randomState.normal(mean, self.sigma)
        return X

    def selectAction(self, state, agent, model):
        """
        Selects an action with exploration.

        Parameters:
            state -- the state for which to select the action
            returns -- the result from the actor's controller plus the
                       exploration
        """
        actions = self.getAction(state, agent, model)
        actions = self._explore(actions)
        return actions

    def getAction(self, state, agent, model):
        """
        Selects an action without exploration.

        Parameters:
            state -- the state for which to select the action
            returns -- the result from the actor's controller
        """
        action = self.actor.forward(np.array([state]))[0]
        
        agent.tuple1 = action[0]
        agent.tuple2 = action[1]
        agent.tuple3 = action[2]
        agent.tuple4 = action[3]
        
        return action

    def getCriticOpinion(self, state):
        """
        Returns the critic's current estimate of the value function for the state.

        Parameters:
            state -- the state the critic shall evaluate
            returns -- the current estimate of the value function for the state
        """      
        return self.critic.forward(np.array([state]))

    def scaleReward(self, reward, maxReward):
        """
        Scales the reward to be within [-1, 1].
        """
        
        reward = reward / float(maxReward) #this was changed from below
        #reward = reward / float(self.maxReward)
        
        # reward = (reward + 1) / 2.0
        return reward

    def reset(self):
        """
        Resets the learner.
        """
        self.sigmas.append(self.sigma)

    def getDataFolderName(self):
        """
        Returns the name of the folder containing the stored data.
        """
        return "%s/results/%s_%s_%s_%s" % (self.folder, self.alpha, self.beta,
                                           self.sigma, self.gamma)

    def finalize(self, folder, index):
        """
        Finalizes this learner, e.g. saves the controllers after training.
        """
        pathName = folder + "/networks"
        self.networkFactory.saveNetwork(self.actor, "actor", pathName, index)
        self.networkFactory.saveNetwork(self.critic, "critic", pathName, index)


class ADCacla(Cacla):
    """
    An Action Dependent implementation of Cacla. The main difference is
    that the critic does not learn the value function but the Q-Function.
    """

    def initController(self):
        """
        Initializes the controllers for the critic and actor.
        """

        criticLayout = (self.actorLayout[0] + self.actorLayout[-1],) + self.actorLayout[1:-1] + (1,)
        self.critic = self.networkFactory.createNetwork("critic",
                criticLayout, self.transferFunctions,
                self.alpha, self.replicationNumber)

        self.actor = self.networkFactory.createNetwork("actor",
                self.actorLayout, self.transferFunctions, self.beta,
                self.replicationNumber)
        self.var = 1.0

    def updateReward(self, state, reward, nextState, action, episodeOver):
        """
        Updated the reward according to the algorithms definition.

        Parameters:
            state -- the current state of the environment
            reward -- the just received reward
            nextState -- the state of the environment after performing the
                         action
            action -- the just performed action
            episodeOver -- True if this was the last step in this episode
        """

        reward = self.scaleReward(reward)

        futureOpinion = self.getCriticOpinion(np.concatenate((nextState, self.getAction(nextState))))
        # ignore the future, if there is none, because this episode is over
        stillRunning = not episodeOver
        target = reward + (stillRunning * self.gamma * futureOpinion)

        criticOpinion = self.getCriticOpinion(np.concatenate((state, action)))
        delta_t = (target - criticOpinion)
        self.errors.append(delta_t)
        numIt = np.ceil(np.nan_to_num(delta_t / np.sqrt(self.var)))
        self.var = self.updateVar(delta_t)
        self.critic.train(np.array([np.concatenate((state, action))]), np.array([target]), 1)
        updateActor = delta_t[0] > 0  # actual performance better than expected
        if updateActor:
            self.actor.train(np.array([state]), np.array([action]), numIt)