# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 14:07:35 2015

@author: boa
"""

from mesa import Agent
from supply_agent import SupplyAgent
from demand_agent import DemandAgent
from technology import Technology

class ElectricityGrid(Agent, Technology, SupplyAgent, DemandAgent):
    def __init__(self, model, unique_id):
        
        #set the basic properties
        self.unique_id = unique_id
        self.type = 'grid'
        self.technology_name = 'grid'

        Technology.__init__(self, model)        
        SupplyAgent.__init__(self)
        DemandAgent.__init__(self)

    def set_electricity_capacity(self,model):
        pass
    
    def set_heat_capacity(self,model):
        pass
    
    def set_electricity_demand(self, model):
        pass
    
    def set_heat_demand(self, model):    
        pass