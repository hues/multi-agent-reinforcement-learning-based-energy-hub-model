# -*- coding: utf-8 -*-
"""
Created on Tue Mar  1 10:44:42 2016

@author: boa
"""

from cacla import Cacla


#TODO:
# does this algorithm feature a continuous state space? if so, adapt the state variable calculations accordingly

class CaclaAgent(object):
    
    def __init__(self, model):
        
        self.learning_agent = True
        
        #initialize some learning agent variables
        self.last_state = None
        self.current_state = None
        self.last_action = None
        self.reward = 0
        
        #initialize the learning method
        if self.type == 'energy conversion technology':
            self.learning_method = Cacla(model.actor_layout, model.transfer_functions, self.unique_id,
                                         model.alpha_cacla, model.beta_cacla, model.gamma_cacla, model.sigma_cacla, model.folder_cacla)
        elif self.type == 'energy storage technology':
            self.learning_method = Cacla(model.actor_layout_storage, model.transfer_functions_storage, self.unique_id,
                                         model.alpha_cacla_storage, model.beta_cacla_storage, model.gamma_cacla_storage, model.sigma_cacla_storage, model.folder_cacla)
            
                                  
        self.learning_method.initController()        
        
    def query_action_dictionary(self, model):
        
        current_increment_normalized = model.current_increment / (model.number_of_increments - 1) * 2 - 1 #normalize to get in range [-1,1]
        
        if self.type =='energy storage technology':       
            
            charge_fraction = self.current_charge / self.storage_capacity
            charge_fraction_normalized = charge_fraction * 2 - 1 #normalize to get in range [-1,1]

            #self.current_state = [model.current_increment, 0.1 * charge_fraction]
            #self.current_state = [current_increment_normalized, charge_fraction_normalized]
            self.current_state = [current_increment_normalized]
       
        elif self.type == 'energy conversion technology':
            #self.current_state = [current_increment_normalized, 0]
            self.current_state = [current_increment_normalized]
        
        if model.schedule.steps <= model.learning_period - 1:
            offer_tuple = self.learning_method.selectAction(self.current_state,self,model) #action selection with exploration
        else:
            offer_tuple = self.learning_method.getAction(self.current_state,self,model) #action selection without exploration
        
        #self.tuple1 = offer_tuple[0]
        #self.tuple2 = offer_tuple[1]
        #self.tuple3 = offer_tuple[2]
        #self.tuple4 = offer_tuple[3]
        #print('normalized state:', self.current_state)
        #if self.technology_name == 'battery' and model.schedule.steps%10 == 0:
            #print(model.schedule.steps, model.current_increment, self.current_state, offer_tuple)
        
        #if self.technology_name == 'battery':
            #print('offer/request tuple:', offer_tuple)
        
        self.last_action = offer_tuple
        
        #print('offer tuple normalized:', offer_tuple)
        
        #de-normalize the offer tuple
        #offer_tuple[0] = offer_tuple[0]
        #offer_tuple[1] = 0.24 * (offer_tuple[1] + 1) / 2
        #offer_tuple[2] = offer_tuple[2]
        #offer_tuple[3] = 0.24 * (offer_tuple[3] + 1) / 2
        
        #print('offer tuple denormalized:', offer_tuple)
        
        return offer_tuple
        
    def set_reward(self, model):
        
        #self.reward = self.profit / model.max_reward
        self.reward = self.profit
        #if self.technology_name == 'battery':        
            #print('profit:', self.profit)
            #print('setting reward:', self.reward)
        #if model.current_increment == model.number_of_increments - 1:        
            #self.reward = self.assets / (model.max_reward * model.number_of_increments)
        #else:
            #self.reward = 0
        
#        if self.last_action[1] < 0 and self.capacity_electricity > 0:
#            self.reward = -1
#            
#        if self.last_action[3] < 0 and self.capacity_heat > 0:
#            self.reward = -1
    
    def update_action_dictionary(self, model):
        
#        if self.technology_name == 'battery':
#            print(' ')
#            print('step:', model.schedule.steps)
#            print('state:', self.current_state)     
#            print('action:', self.last_action)
#            print('reward:', self.reward)
        
        #set the last state as the current state        
        self.last_state = self.current_state
        
        current_increment_normalized = model.current_increment / (model.number_of_increments - 1) * 2 - 1 #normalize to get in range [-1,1]
        next_increment_normalized = (model.current_increment + 1) / (model.number_of_increments - 1) * 2 - 1 #normalize to get in range [-1,1]
        zero_increment_normalized = -1 #normalize to get in range [-1,1]

        #set the current state as the next state        
        if self.type =='energy storage technology':
            
            charge_fraction = self.current_charge / self.storage_capacity
            charge_fraction_normalized = charge_fraction * 2 - 1 #normalize to get in range [-1,1]
            initial_charge_fraction_normalized = self.initial_charge / self.storage_capacity * 2 - 1
            
            if model.current_increment == model.number_of_increments - 1:
                #self.current_state = [0, self.initial_charge / self.storage_capacity]
                #self.current_state = [zero_increment_normalized, initial_charge_fraction_normalized]
                self.current_state = [zero_increment_normalized]
            else:
                #self.current_state = [model.current_increment + 1, charge_fraction]
                #self.current_state = [next_increment_normalized, charge_fraction_normalized]
                self.current_state = [next_increment_normalized]
        
        elif self.type == 'energy conversion technology':
            #self.current_state = [(model.current_increment + 1) % model.number_of_increments, 0]
            #self.current_state = [current_increment_normalized, 0]
            self.current_state = [current_increment_normalized]

        episode_over = False            
        if model.current_increment == model.number_of_increments - 1:
            episode_over = True   
        
        #print('normalized state current:', self.last_state)
        #print('normalized state next:', self.current_state)
        #if self.technology_name == 'battery':
            #print('current state:', self.last_state, 'last action:', self.last_action)
            #print('next state:', self.current_state)
        self.learning_method.updateReward(self.last_state, self.reward, self.current_state, self.last_action, episode_over, self)
        
                                     
                    



