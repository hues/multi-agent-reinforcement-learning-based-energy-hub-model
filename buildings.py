# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 09:57:28 2015

@author: boa
"""

from mesa import Agent
from demand_agent import DemandAgent
from technology import Technology

class BuildingAgent(Agent, Technology, DemandAgent):
    def __init__(self, model, unique_id):
        
        #set the basic properties
        self.unique_id = unique_id
        self.type = 'building'
        self.technology_name = 'building'
        
        #initialize the current heat and electricity demand variables
        self.current_electricity_demand = 0
        self.current_heat_demand = 0
        
        DemandAgent.__init__(self)
        Technology.__init__(self, model)

    def set_electricity_demand(self, model):
        self.current_electricity_demand = model.electricity_demand_per_building_agent[model.current_increment] 

    def set_heat_demand(self, model):    
        self.current_heat_demand = model.heat_demand_per_building_agent[model.current_increment]