# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 15:48:18 2015

@author: boa
"""

from learning_actions import QLearnAgent
from learning_actions_cacla import CaclaAgent

class DemandAgent(object):
    def __init__(self):
        self.demand_agent = True
        self.request_tuple = None

    def place_request(self, model, request_type):
        
        #building heat and electricity demands are predefined
        if self.type == 'building':
            self.electricity_request_quantity = self.current_electricity_demand
            self.electricity_request_price = model.grid_electricity_price[model.current_increment]
            self.heat_request_quantity = self.current_heat_demand
            self.heat_request_price = model.grid_electricity_price[model.current_increment]
        
        #grid heat and electricity demands are predefined
        elif self.type == 'grid':
            self.electricity_request_quantity = float("inf")
            self.electricity_request_price = model.grid_electricity_offer_price[model.current_increment]
            self.heat_request_quantity = 0
            self.heat_request_price = 0         
        
        #the electricity demand of some energy conversion technologies (e.g. heat pump)
        #is determined by their heat production
        elif self.type == 'energy conversion technology':
            self.electricity_request_quantity = self.current_electricity_demand
            self.electricity_request_price = model.grid_electricity_price[model.current_increment]
            self.heat_request_quantity = 0
            self.heat_request_price = 0
        
        #heat and electricity demands of all other agents are based on learning outcomes
        elif self.type == 'energy storage technology':
                        
            #if (request_type == 'electricity' and self.technology_name == 'battery') or (request_type == 'heat' and self.technology_name == 'hot water tank'):
                
            if self.request_tuple == None:
                
                if model.learning_algorithm == 1:               
                    self.request_tuple = QLearnAgent.query_action_dictionary(self, model)
                else:
                    self.request_tuple = CaclaAgent.query_action_dictionary(self, model)
                    
                #print(model.schedule.steps, model.current_interval, self.request_tuple)
                
                if model.print_stuff == True:
                    print(' ')
                    print('Action dictionary request values of', self.technology_name, self.unique_id, 'are', self.request_tuple)
            
            #print(self.request_tuple)            
            
            if model.learning_algorithm == 1:  
                if self.request_tuple[0] < 0:
                    self.electricity_request_quantity = self.request_tuple[0] * -1 * self.current_purchase_capacity_electricity / 100
                    self.electricity_request_price = self.request_tuple[1]
                else:
                    self.electricity_request_quantity = 0
                    self.electricity_request_price = 0
                    
                if self.request_tuple[2] < 0:            
                    self.heat_request_quantity = self.request_tuple[2] * -1  * self.current_purchase_capacity_heat / 100
                    self.heat_request_price = self.request_tuple[3]
                else:
                    self.heat_request_quantity = 0
                    self.heat_request_price = 0
            else:
                if self.request_tuple[0] < 0:
                    self.electricity_request_quantity = min(-1 * self.request_tuple[0] * self.current_purchase_capacity_electricity, self.current_purchase_capacity_electricity)
                    #self.electricity_request_quantity = self.current_purchase_capacity_electricity
                    #self.electricity_request_price = self.request_tuple[1]
                    self.electricity_request_price = 0.24 * (self.request_tuple[1] + 1) / 2
                    #self.electricity_request_price = 0.14
                else:
                    self.electricity_request_quantity = 0
                    self.electricity_request_price = 0
                    
                if self.request_tuple[2] < 0:            
                    self.heat_request_quantity = min(-1 * self.request_tuple[2] * self.current_purchase_capacity_heat, self.current_purchase_capacity_heat)
                    #self.heat_request_quantity = self.current_purchase_capacity_heat
                    #self.heat_request_price = self.request_tuple[3]
                    self.heat_request_price = 0.24 * (self.request_tuple[3] + 1) / 2
                else:
                    self.heat_request_quantity = 0
                    self.heat_request_price = 0
                
                
            if model.print_stuff == True:
                print('Request is', self.electricity_request_quantity, self.electricity_request_price, self.heat_request_quantity, self.heat_request_price)
                    
            #if self.technology_name == 'battery' and request_type == 'electricity':
                #print(' ')
                #print('step:', model.schedule.steps, ', increment:', model.current_increment)
                #print('charge:', self.current_charge)
                #print('current capacity:', self.current_capacity_electricity)
                    #print('electricity request quantity:', self.electricity_request_quantity, 'electricity request price:', self.electricity_request_price)
        
        else:
            print('WARNING: Cannot place request for', self.type, self.unique_id)
        
        if request_type == 'electricity':
            electricity_request = [self.electricity_request_quantity, self.electricity_request_price]
            if model.print_stuff == True:
                print(self.technology_name,self.unique_id,"placing electricity request",electricity_request)
            return electricity_request
        
        elif request_type == 'heat':
            heat_request = [self.heat_request_quantity, self.heat_request_price]
            if model.print_stuff == True:            
                print(self.technology_name,self.unique_id,"placing heat request",heat_request)
            return heat_request