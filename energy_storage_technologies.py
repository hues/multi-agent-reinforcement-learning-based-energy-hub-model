# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 11:30:51 2015

@author: boa
"""

from mesa import Agent    
from supply_agent import SupplyAgent
from demand_agent import DemandAgent
from technology import Technology
from learning_actions import QLearnAgent
from learning_actions_cacla import CaclaAgent


class Battery(Agent, Technology, SupplyAgent, DemandAgent):
    def __init__(self, model, unique_id):
        
        #set basic technology properties        
        self.unique_id = unique_id
        self.type = 'energy storage technology'
        self.technology_name = 'battery'
        
        self.storage_capacity = 100
        capital_cost_per_kWh = 100
        lifetime = 20
        self.charging_efficiency = 0.9
        self.discharging_efficiency = 0.9
        self.decay = 0.001 #TODO: Check if this is accounted for in the charge state of the battery
        self.max_charging_rate = 0.1 #0.3 #dimensionless
        self.max_discharging_rate = 0.1 #0.3 #dimensionless
        self.min_storage_state = 0.3 #dimensionless
        
        #initialize the charge
        #self.initial_charge = self.storage_capacity * self.min_storage_state
        self.initial_charge = self.storage_capacity * 0.5
        self.current_charge = self.initial_charge
        
        #set the capacity        
        self.capacity_electricity = self.storage_capacity * self.max_discharging_rate * self.discharging_efficiency
        self.capacity_heat = 0
        self.current_capacity_heat = self.capacity_heat 
        self.current_capacity_electricity = self.capacity_electricity
        
        #set the purchase capacity
        self.purchase_capacity_electricity = self.storage_capacity * self.max_charging_rate / self.charging_efficiency
        self.purchase_capacity_heat = 0
        self.current_purchase_capacity_heat = self.purchase_capacity_heat
        self.current_purchase_capacity_electricity = self.purchase_capacity_electricity
        
        #set the marginal and fixed costs
        self.marginal_cost_heat = 0
        self.marginal_cost_electricity = 0
        self.previous_marginal_cost = 0
        present_value_of_annuity_factor = model.interest_rate / (1 - (1 / ((1 + model.interest_rate)**(lifetime))))
        fixed_cost_per_year = capital_cost_per_kWh * self.storage_capacity * present_value_of_annuity_factor
        self.fixed_cost_per_hour = fixed_cost_per_year / 8760
        
        SupplyAgent.__init__(self)
        DemandAgent.__init__(self)
        Technology.__init__(self, model)
        
        if model.learning_algorithm == 1:
            QLearnAgent.__init__(self, model)
        else:
            CaclaAgent.__init__(self, model)
        
    def set_electricity_capacity(self,model):
        
        #calculate the current capacity
        charge_floor = self.min_storage_state * self.storage_capacity
        max_discharge = self.capacity_electricity
        if self.current_charge >= charge_floor + max_discharge:
            self.current_capacity_electricity = max_discharge
        elif self.current_charge >= charge_floor:
            self.current_capacity_electricity = self.current_charge - charge_floor
        else:
            self.current_capacity_electricity = 0
        
        if model.print_stuff == True:
            print('Current charge of battery is', self.current_charge, '. Current capacity is', self.current_capacity_electricity)
        
    def set_electricity_demand(self, model):
        
        #calculate the current purchase capacity
        charge_ceiling = self.storage_capacity
        max_charge = self.purchase_capacity_electricity
        if self.current_charge <= charge_ceiling - max_charge:
            self.current_purchase_capacity_electricity = max_charge
        elif self.current_charge <= charge_ceiling:
            self.current_purchase_capacity_electricity = charge_ceiling - self.current_charge
        else:
            self.current_purchase_capacity_electricity = 0
        
        if model.print_stuff == True:
            print('Current purchase capacity of battery is', self.current_purchase_capacity_electricity)
            
    def set_heat_capacity(self,model):
        pass
    
    def set_heat_demand(self, model):
        pass
        
    def set_marginal_costs(self, market):
        
        #marginal cost is the mean purchase cost per kWh of all the energy currently stored in the device
        self.marginal_cost = self.previous_marginal_cost
        cost_of_existing_energy_stored = self.marginal_cost *  self.current_charge / (1 - self.decay)
        cost_of_energy_sold = self.marginal_cost * self.electricity_produced / self.discharging_efficiency
        cost_of_new_energy_purchased = self.electricity_request_price * self.electricity_purchased / self.charging_efficiency
        #cost_of_new_energy_purchased = market.electricity_clearing_price * self.electricity_purchased / self.charging_efficiency
        new_state_of_charge = self.current_charge * (1 - self.decay) + self.electricity_purchased * self.charging_efficiency - self.electricity_produced / self.discharging_efficiency
        if new_state_of_charge > 0:        
            new_marginal_cost = (cost_of_existing_energy_stored + cost_of_new_energy_purchased - cost_of_energy_sold) / new_state_of_charge
            self.previous_marginal_cost = new_marginal_cost
    
    def set_charge_state(self):
        self.current_charge = self.current_charge * (1 - self.decay) + self.electricity_purchased * self.charging_efficiency - self.electricity_produced / self.discharging_efficiency
    
    def reset_charge_state(self):    
        self.current_charge = self.initial_charge
        
    def reset_assets(self):
        self.assets = 0
    
class HotWaterTank(Agent, Technology, SupplyAgent, DemandAgent):
    def __init__(self, model, unique_id):
        
        #set basic technology properties
        self.unique_id = unique_id
        self.type = 'energy storage technology'
        self.technology_name = 'hot water tank'
        
        self.storage_capacity = 100
        capital_cost_per_kWh = 100
        lifetime = 17
        self.charging_efficiency = 0.9
        self.discharging_efficiency = 0.9
        self.decay = 0.01
        self.max_charging_rate = 0.25 #dimensionless
        self.max_discharging_rate = 0.25 #dimensionless
        self.min_storage_state = 0 #dimensionless
        
        #initialize the charge
        #self.initial_charge = self.storage_capacity * self.min_storage_state
        self.initial_charge = self.storage_capacity * 0.5
        self.current_charge = self.initial_charge
        
        #set the capacity        
        self.capacity_electricity = 0
        self.capacity_heat = self.storage_capacity * self.max_discharging_rate * self.discharging_efficiency
        self.current_capacity_heat = self.capacity_heat 
        self.current_capacity_electricity = self.capacity_electricity
        
        #set the purchase capacity
        self.purchase_capacity_electricity = 0
        self.purchase_capacity_heat = self.storage_capacity * self.max_charging_rate / self.charging_efficiency
        self.current_purchase_capacity_heat = self.purchase_capacity_heat
        self.current_purchase_capacity_electricity = self.purchase_capacity_electricity
        
        #set the marginal and fixed costs
        self.marginal_cost_heat = 0
        self.marginal_cost_electricity = 0
        self.previous_marginal_cost = 0
        present_value_of_annuity_factor = model.interest_rate / (1 - (1 / ((1 + model.interest_rate)**(lifetime))))
        fixed_cost_per_year = capital_cost_per_kWh * self.storage_capacity * present_value_of_annuity_factor
        self.fixed_cost_per_hour = fixed_cost_per_year / 8760
        
        SupplyAgent.__init__(self)
        DemandAgent.__init__(self)
        Technology.__init__(self, model)
        
        if model.learning_algorithm == 1:
            QLearnAgent.__init__(self, model)
        else:
            CaclaAgent.__init__(self, model)
        
    def set_electricity_capacity(self,model):
        pass
    
    def set_electricity_demand(self, model):
        pass
            
    def set_heat_capacity(self,model):
        
        #calculate the current capacity
        charge_floor = self.min_storage_state * self.storage_capacity
        max_discharge = self.capacity_heat
        if self.current_charge >= charge_floor + max_discharge:
            self.current_capacity_heat = max_discharge
        elif self.current_charge >= charge_floor:
            self.current_capacity_heat = self.current_charge - charge_floor
        else:
            self.current_capacity_heat = 0
         
        if model.print_stuff == True:
            print(' ')
            print('Current charge of heat tank is', self.current_charge, '. Current capacity is', self.current_capacity_heat)
            
    def set_heat_demand(self, model):
        
        #calculate the current purchase capacity
        charge_ceiling = self.storage_capacity
        max_charge = self.purchase_capacity_heat
        if self.current_charge <= charge_ceiling - max_charge:
            self.current_purchase_capacity_heat = max_charge
        elif self.current_charge <= charge_ceiling:
            self.current_purchase_capacity_heat = charge_ceiling - self.current_charge
        else:
            self.current_purchase_capacity_heat = 0
        
        if model.print_stuff == True:
            print('Current purchase capacity of heat tank is', self.current_purchase_capacity_heat)
        
    def set_marginal_costs(self, market):
        
        #marginal cost is the mean purchase cost per kWh of all the energy currently stored in the device
        self.marginal_cost = self.previous_marginal_cost
        cost_of_existing_energy_stored = self.marginal_cost *  self.current_charge / (1 - self.decay)
        cost_of_energy_sold = self.marginal_cost * self.heat_produced / self.discharging_efficiency
        cost_of_new_energy_purchased = self.heat_request_price * self.heat_purchased / self.charging_efficiency
        #cost_of_new_energy_purchased = market.heat_clearing_price * self.heat_purchased / self.charging_efficiency
        new_state_of_charge = self.current_charge * (1 - self.decay) + self.heat_purchased * self.charging_efficiency - self.heat_produced / self.discharging_efficiency
        if new_state_of_charge > 0:        
            new_marginal_cost = (cost_of_existing_energy_stored + cost_of_new_energy_purchased - cost_of_energy_sold) / new_state_of_charge
            self.previous_marginal_cost = new_marginal_cost
    
    def set_charge_state(self):
        self.current_charge = self.current_charge * (1 - self.decay) + self.heat_purchased * self.charging_efficiency - self.heat_produced / self.discharging_efficiency

    def reset_charge_state(self):    
        self.current_charge = self.initial_charge
        
    def reset_assets(self):
        self.assets = 0