# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 09:49:40 2015

@author: boa
"""

from mesa import Agent    
from supply_agent import SupplyAgent
from demand_agent import DemandAgent
from technology import Technology
from learning_actions import QLearnAgent
from learning_actions_cacla import CaclaAgent

class HeatPump(Agent, Technology, SupplyAgent, DemandAgent):
    
    def __init__(self,model,unique_id):
        
        #set basic technology properties
        self.unique_id = unique_id
        self.type = 'energy conversion technology'
        self.technology_name = 'heat pump'
        self.capacity_heat = 100
        self.capacity_electricity = 0
        capital_cost_per_kW = 1000
        self.OMV_cost = 0.1
        self.efficiency = 3.2        
        lifetime = 20
        self.minimum_load = 0
        
        #set the purchase capacity
        self.purchase_capacity_electricity = self.capacity_heat / self.efficiency
        self.purchase_capacity_heat = 0
        
        #initialize the current capacity
        self.current_capacity_heat = self.capacity_heat 
        self.current_capacity_electricity = self.capacity_electricity
        
        #initialize the current demand
        self.current_electricity_demand = 0
        self.current_heat_demand = 0
        
        #set the marginal and fixed costs
        self.marginal_cost_heat = 0 #dynamically determined
        self.marginal_cost_electricity = 0
        present_value_of_annuity_factor = model.interest_rate / (1 - (1 / ((1 + model.interest_rate)**(lifetime))))
        fixed_cost_per_year = capital_cost_per_kW * self.capacity_heat * present_value_of_annuity_factor
        self.fixed_cost_per_hour = fixed_cost_per_year / 8760
        
        SupplyAgent.__init__(self)
        DemandAgent.__init__(self)
        Technology.__init__(self, model)
        
        if model.learning_algorithm == 1:
            QLearnAgent.__init__(self, model)
        else:
            CaclaAgent.__init__(self, model)
        
    def set_electricity_capacity(self,model):
        pass
    
    def set_electricity_demand(self, model):
        self.current_electricity_demand = self.heat_produced / self.efficiency
    
    def set_heat_capacity(self,model):
        pass
    
    def set_heat_demand(self, model):
        pass
        
    def set_marginal_costs(self, market):
        
        #update the marginal costs
        operating_cost_per_kWh = self.electricity_request_price / self.efficiency
        #operating_cost_per_kWh = market.electricity_clearing_price / self.efficiency        
        self.marginal_cost_heat = operating_cost_per_kWh + self.OMV_cost
        
    def reset_assets(self):
        self.assets = 0


class GasBoiler(Agent, Technology, SupplyAgent):
    
    def __init__(self,model,unique_id):
        
        #set basic technology properties
        self.unique_id = unique_id
        self.type = 'energy conversion technology'
        self.technology_name = 'gas boiler'
        self.capacity_heat = 100
        self.capacity_electricity = 0
        capital_cost_per_kW = 200
        OMV_cost = 0.01
        efficiency = 0.94
        operating_cost_per_kWh = model.natural_gas_price / efficiency
        lifetime = 30
        self.minimum_load = 0
        
        #initialize the current capacity
        self.current_capacity_heat = self.capacity_heat 
        self.current_capacity_electricity = self.capacity_electricity
        
        #set the marginal and fixed costs
        self.marginal_cost_heat = operating_cost_per_kWh + OMV_cost
        self.marginal_cost_electricity = 0
        present_value_of_annuity_factor = model.interest_rate / (1 - (1 / ((1 + model.interest_rate)**(lifetime))))
        fixed_cost_per_year = capital_cost_per_kW * self.capacity_heat * present_value_of_annuity_factor
        self.fixed_cost_per_hour = fixed_cost_per_year / 8760
        
        SupplyAgent.__init__(self)
        Technology.__init__(self, model)
        
        if model.learning_algorithm == 1:
            QLearnAgent.__init__(self, model)
        else:
            CaclaAgent.__init__(self, model)

    def set_electricity_capacity(self,model):
        pass
    
    def set_heat_capacity(self,model):
        pass
        
    def set_marginal_costs(self, market):
        pass
    
    def reset_assets(self):
        self.assets = 0


class CHP(Agent, Technology, SupplyAgent):
    
    def __init__(self,model,unique_id):
        
        #set basic technology properties
        self.unique_id = unique_id
        self.type = 'energy conversion technology'
        self.technology_name = 'CHP'
        self.capacity_electricity = 50
        self.heat_to_power_ratio = 1.73        
        self.capacity_heat = self.heat_to_power_ratio * self.capacity_electricity
        capital_cost_per_kW = 1500
        efficiency_electricity = 0.3        
        #efficiency_heat = efficiency_electricity * self.heat_to_power_ratio
        #efficiency = efficiency_heat + efficiency_electricity
        operating_cost_per_kWh = model.natural_gas_price / efficiency_electricity
        OMV_cost = 0.021
        lifetime = 20
        self.minimum_load = 0.5
        
        #initialize the current capacity
        self.current_capacity_heat = self.capacity_heat 
        self.current_capacity_electricity = self.capacity_electricity
        
        #set the marginal and fixed costs
        self.marginal_cost_heat = (operating_cost_per_kWh  + OMV_cost) / self.heat_to_power_ratio
        #self.marginal_cost_electricity = operating_cost_per_kWh + OMV_cost
        self.marginal_cost_electricity = 0
        present_value_of_annuity_factor = model.interest_rate / (1 - (1 / ((1 + model.interest_rate)**(lifetime))))
        fixed_cost_per_year = capital_cost_per_kW * self.capacity_electricity * present_value_of_annuity_factor
        self.fixed_cost_per_hour = fixed_cost_per_year / 8760
        
        SupplyAgent.__init__(self)
        Technology.__init__(self, model)
        
        if model.learning_algorithm == 1:
            QLearnAgent.__init__(self, model)
        else:
            CaclaAgent.__init__(self, model)

    def set_electricity_capacity(self,model):
        
        #calculate the electricity capacity based on the heat produced
        self.current_capacity_electricity = self.heat_produced / self.heat_to_power_ratio
    
    def set_heat_capacity(self,model):
        pass
        
    def set_marginal_costs(self, market):
        pass
    
    def reset_assets(self):
        self.assets = 0


class SolarPV(Agent, Technology, SupplyAgent):
    
    def __init__(self,model,unique_id):
        
        #set basic technology properties
        self.unique_id = unique_id
        self.type = 'energy conversion technology'
        self.technology_name = 'solar PV'
        capacity_per_area = 0.125
        self.area = 200 #THIS HAS BEEN CHANGED FROM THE GENERIC ENERGY HUB
        self.efficiency = 0.14
        self.capacity_electricity = capacity_per_area * self.area
        self.capacity_heat = 0        
        capital_cost_per_kW = 3500
        operating_cost_per_kWh = 0        
        OMV_cost = 0.06
        lifetime = 20
        self.minimum_load = 0
        
        #initialize the current capacity
        self.current_capacity_heat = self.capacity_heat 
        self.current_capacity_electricity = self.capacity_electricity
        
        #set the marginal and fixed costs
        self.marginal_cost_heat = 0
        self.marginal_cost_electricity = operating_cost_per_kWh + OMV_cost
        present_value_of_annuity_factor = model.interest_rate / (1 - (1 / ((1 + model.interest_rate)**(lifetime))))
        fixed_cost_per_year = capital_cost_per_kW * self.capacity_electricity * present_value_of_annuity_factor
        self.fixed_cost_per_hour = fixed_cost_per_year / 8760
        
        SupplyAgent.__init__(self)
        Technology.__init__(self, model)
        
        if model.learning_algorithm == 1:
            QLearnAgent.__init__(self, model)
        else:
            CaclaAgent.__init__(self, model)

    def set_electricity_capacity(self,model):
        
        #calculate the electricity capacity based on the current solar insolation
        #available_electricity = self.area * model.solar_insolation[model.current_increment] * self.efficiency
        #self.current_capacity_electricity = min(self.capacity_electricity, available_electricity)
        self.current_capacity_electricity = model.solar_insolation[model.current_increment] * self.area * self.efficiency
        
    def set_heat_capacity(self,model):
        pass
        
    def set_marginal_costs(self, market):
        pass
            
    def reset_assets(self):
        self.assets = 0
        
        
class SolarThermal(Agent, Technology, SupplyAgent):
    
    def __init__(self,model,unique_id):
        
        #set basic technology properties
        self.unique_id = unique_id
        self.type = 'energy conversion technology'
        self.technology_name = 'solar thermal'
        capacity_per_area = 0.7
        self.area = 200
        self.efficiency = 0.46
        self.capacity_heat = capacity_per_area * self.area
        self.capacity_electricity = 0
        capital_cost_per_kW = 2900
        operating_cost_per_kWh = 0
        OMV_cost = 0.12
        lifetime = 35
        self.minimum_load = 0
        
        #initialize the current capacity
        self.current_capacity_heat = self.capacity_heat 
        self.current_capacity_electricity = self.capacity_electricity
        
        #set the marginal and fixed costs
        self.marginal_cost_heat = operating_cost_per_kWh + OMV_cost
        self.marginal_cost_electricity = 0        
        present_value_of_annuity_factor = model.interest_rate / (1 - (1 / ((1 + model.interest_rate)**(lifetime))))
        fixed_cost_per_year = capital_cost_per_kW * self.capacity_heat * present_value_of_annuity_factor
        self.fixed_cost_per_hour = fixed_cost_per_year / 8760
        
        SupplyAgent.__init__(self)
        Technology.__init__(self, model)
        
        if model.learning_algorithm == 1:
            QLearnAgent.__init__(self, model)
        else:
            CaclaAgent.__init__(self, model)

    def set_electricity_capacity(self,model):
        pass
    
    def set_heat_capacity(self,model):
        
        #calculate the electricity capacity based on the current solar insolation
        #available_heat = self.area * model.solar_insolation[model.current_increment] * self.efficiency 
        #self.current_capacity_heat = min(self.capacity_heat, available_heat) 
        self.current_capacity_heat = model.solar_insolation[model.current_increment] * self.area * self.efficiency

    def set_marginal_costs(self, market):
        pass
    
    def reset_assets(self):
        self.assets = 0
                                    