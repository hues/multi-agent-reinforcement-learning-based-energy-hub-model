# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 15:53:16 2015

@author: boa
"""
from learning_actions import QLearnAgent
from learning_actions_cacla import CaclaAgent

class SupplyAgent(object):
    def __init__(self):
        self.supply_agent = True
        self.offer_tuple = None

    def place_offer(self, model, offer_type):
        
        #heat and electricity offers of the grid are predefined
        if self.type == 'grid':
            self.electricity_offer_quantity = float("inf")
            #self.electricity_offer_quantity = model.electricity_demand_per_building_agent[model.current_increment] * model.number_of_buildings
            self.electricity_offer_price = model.grid_electricity_price[model.current_increment]
            self.heat_offer_quantity = 0
            self.heat_offer_price = 0    
        
        elif self.type == 'energy conversion technology':
            
            if self.offer_tuple == None:
                if model.learning_algorithm == 1:
                    self.offer_tuple = QLearnAgent.query_action_dictionary(self, model)
                else:
                    self.offer_tuple = CaclaAgent.query_action_dictionary(self, model)
            
            self.electricity_offer_quantity = self.current_capacity_electricity
            self.electricity_offer_price = self.offer_tuple[1]
            self.heat_offer_quantity = self.current_capacity_heat
            self.heat_offer_price = self.offer_tuple[3]
        
        #offers of all other supply agents are based on learning outcomes        
        elif self.type == 'energy storage technology': 
            
            if self.offer_tuple == None:
                
                self.offer_tuple = self.request_tuple #for energy storage technologies, the offer tuple is identical to the request tuple           
                
                if model.print_stuff == True:
                    print(' ')
                    print('Action dictionary offer values of', self.technology_name, self.unique_id, 'are', self.offer_tuple)
        
            if model.learning_algorithm == 1:
                if self.offer_tuple[0] > 0:
                    self.electricity_offer_quantity = self.offer_tuple[0] * self.current_capacity_electricity / 100
                    self.electricity_offer_price = self.offer_tuple[1]
                else:
                    self.electricity_offer_quantity = 0
                    self.electricity_offer_price = 0
                    
                if self.offer_tuple[2] > 0:
                    self.heat_offer_quantity = self.offer_tuple[2] * self.current_capacity_heat / 100
                    self.heat_offer_price = self.offer_tuple[3]
                else:
                    self.heat_offer_quantity = 0
                    self.heat_offer_price = 0
            else:
                if self.offer_tuple[0] > 0:
                    self.electricity_offer_quantity = min(self.offer_tuple[0] * self.current_capacity_electricity, self.current_capacity_electricity)
                    #self.electricity_offer_quantity = self.current_capacity_electricity
                    #self.electricity_offer_price = self.offer_tuple[1]
                    self.electricity_offer_price = 0.24 * (self.offer_tuple[1] + 1) / 2
                    #self.electricity_offer_price = 0.23
                else:
                    self.electricity_offer_quantity = 0
                    self.electricity_offer_price = 0
                    
                if self.offer_tuple[2] > 0:
                    self.heat_offer_quantity = min(self.offer_tuple[2] * self.current_capacity_heat, self.current_capacity_heat)
                    #self.heat_offer_quantity = self.current_capacity_heat
                    #self.heat_offer_price = self.offer_tuple[3]
                    self.heat_offer_price = 0.24 * (self.offer_tuple[3] + 1) / 2
                else:
                    self.heat_offer_quantity = 0
                    self.heat_offer_price = 0
                
        
                
            if model.print_stuff == True:
                print('Offer is', self.electricity_offer_quantity, self.electricity_offer_price, self.heat_offer_quantity, self.heat_offer_price)
                    
            #print(self.technology_name, 'offer tuple:', self.request_tuple)
            #if self.technology_name == 'battery' and offer_type == 'electricity':        
                #print('electricity offer quantity:', self.electricity_offer_quantity, 'electricity offer price:', self.electricity_offer_price)

            
        else:
            print('WARNING: Cannot place request for', self.type, self.unique_id)
            
        if offer_type == 'electricity':
            electricity_offer = [self.electricity_offer_quantity, self.electricity_offer_price]
            if model.print_stuff == True:
                print(self.technology_name,self.unique_id,"placing offer",electricity_offer)
            return electricity_offer
        
        elif offer_type == 'heat':
            heat_offer = [self.heat_offer_quantity, self.heat_offer_price] 
            if model.print_stuff == True:
                print(self.technology_name,self.unique_id,"placing offer",heat_offer)
            return heat_offer