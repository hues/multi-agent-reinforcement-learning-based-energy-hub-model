# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 10:03:50 2015

@author: boa
"""

from mesa import Agent
import operator
from random import shuffle

class MarketAgent(Agent):
    def __init__(self, unique_id):
        
        #initialize some variables
        self.unique_id = unique_id
        self.type = 'market'
        self.electricity_offer_quantity_dictionary = {}
        self.electricity_offer_price_dictionary = {}
        self.electricity_request_quantity_dictionary = {}
        self.electricity_request_price_dictionary = {}
        self.electricity_produced_dictionary = {}
        self.electricity_purchased_dictionary = {}
        self.remaining_electricity_supply_dictionary = {}
        self.electricity_clearing_price = None
        self.total_electricity_demand = 0
        self.electricity_clearing_price_list = []
        self.heat_offer_quantity_dictionary = {}
        self.heat_offer_price_dictionary = {}
        self.heat_request_quantity_dictionary = {}
        self.heat_request_price_dictionary = {}
        self.heat_produced_dictionary = {}
        self.heat_purchased_dictionary = {}
        self.remaining_heat_supply_dictionary = {}
        self.heat_clearing_price = None
        self.total_heat_demand = 0
        self.heat_clearing_price_list = []
        
    def aggregate_electricity_requests(self, model):
        
        if model.print_stuff == True:
            print(' ')
            print("MARKET AGENT AGGREGATING ELECTRICITY REQUESTS")
        
        for agent in model.schedule.agents:
            if hasattr(agent, 'demand_agent'):
                agent_electricity_request = agent.place_request(model,'electricity')
                
                #if the request quantity > 0, add it to the market list
                if agent_electricity_request[0] > 0:
                    self.electricity_request_quantity_dictionary[agent.unique_id] = agent_electricity_request[0]
                    self.electricity_request_price_dictionary[agent.unique_id] = agent_electricity_request[1]
    
    def aggregate_heat_requests(self, model):
        
        if model.print_stuff == True:
            print(' ')
            print("MARKET AGENT AGGREGATING HEAT REQUESTS")
        
        for agent in model.schedule.agents:
            if hasattr(agent, 'demand_agent'):
                agent_heat_request = agent.place_request(model,'heat')
                
                #if the request quantity > 0, add it to the market list
                if agent_heat_request[0] > 0:
                    self.heat_request_quantity_dictionary[agent.unique_id] = agent_heat_request[0]
                    self.heat_request_price_dictionary[agent.unique_id] = agent_heat_request[1]

    def aggregate_electricity_offers(self, model):
        
        if model.print_stuff == True:
            print(' ')
            print("MARKET AGENT AGGREGATING ELECTRICITY OFFERS")
        
        for agent in model.schedule.agents:
            if hasattr(agent, 'supply_agent'):
                agent_electricity_offer = agent.place_offer(model,'electricity')
                
                #if the offer quantity > 0, add it to the market list
                if agent_electricity_offer[0] > 0:                
                    self.electricity_offer_quantity_dictionary[agent.unique_id] = agent_electricity_offer[0]
                    self.electricity_offer_price_dictionary[agent.unique_id] = agent_electricity_offer[1]

    def aggregate_heat_offers(self, model):
        
        if model.print_stuff == True:
            print(' ')
            print("MARKET AGENT AGGREGATING HEAT OFFERS")
        
        for agent in model.schedule.agents:
            if hasattr(agent, 'supply_agent'):
                agent_heat_offer = agent.place_offer(model,'heat')
                
                #if the offer quantity > 0, add it to the market list
                if agent_heat_offer[0] > 0:                    
                    self.heat_offer_quantity_dictionary[agent.unique_id] = agent_heat_offer[0]
                    self.heat_offer_price_dictionary[agent.unique_id] = agent_heat_offer[1]
                
    def clear_electricity_market(self, model):
        if model.print_stuff == True:
            print(' ')
            print("MARKET AGENT CLEARING ELECTRICITY MARKET")
        
        #get a list of the demand agent ids sorted by decreasing request price
        sorted_request_prices = sorted(self.electricity_request_price_dictionary.items(), key=operator.itemgetter(1), reverse=True)
        sorted_request_keys = []
        sorted_request_prices_list = []
        for i in sorted_request_prices:
            sorted_request_keys.append(i[0])
            sorted_request_prices_list.append(i[1])
        if model.print_stuff == True:
            print('purchasing agents:', sorted_request_keys)
            
        #randomize the order of the agents with identical request prices
        #go through the list of request prices. if you find multiple identical values in a row
        #randomize the order of the corresponding keys with that value
        if model.print_stuff == True:
            print(' ')    
            print('Electricity request prices:',sorted_request_prices_list)
            print('Electricity request keys:',sorted_request_keys)
        
        q = 0
        for i in sorted_request_prices_list:
            identical_keys = []
            identical_keys_indices = []
            identical_keys.append(sorted_request_keys[q])
            identical_keys_indices.append(q)
            
            r = 0
            for j in sorted_request_prices_list:
                if i == j:
                    if sorted_request_keys[q] != sorted_request_keys[r]: 
                        identical_keys.append(sorted_request_keys[r])
                        identical_keys_indices.append(r)
                        
                r+=1
            if len(identical_keys) > 1:
                shuffle(identical_keys)
                s = 0
                for k in identical_keys_indices:
                    sorted_request_keys[k] = identical_keys[s]
                    s+=1
            q+=1
            
        if model.print_stuff == True:
            print('Electricity request keys (resorted):',sorted_request_keys)
        
        #get a list of the supply agent ids sorted by increasing offer price
        sorted_offer_prices = sorted(self.electricity_offer_price_dictionary.items(), key=operator.itemgetter(1))
        sorted_offer_keys = []
        sorted_offer_prices_list = []
        for i in sorted_offer_prices:
            sorted_offer_keys.append(i[0])
            sorted_offer_prices_list.append(i[1])
        if model.print_stuff == True:
            print('offering agents:', sorted_offer_keys)
            
        #randomize the order of the agents with identical request prices
        #go through the list of request prices. if you find multiple identical values in a row
        #randomize the order of the corresponding keys with that value
        if model.print_stuff == True:
            print(' ')    
            print('Electricity offer prices:',sorted_offer_prices_list)
            print('Electricity offer keys:',sorted_offer_keys)            
        
        q = 0
        for i in sorted_offer_prices_list:
            identical_keys = []
            identical_keys_indices = []
            identical_keys.append(sorted_offer_keys[q])
            identical_keys_indices.append(q)
            
            r = 0
            for j in sorted_offer_prices_list:
                if i == j:
                    if sorted_offer_keys[q] != sorted_offer_keys[r]: 
                        identical_keys.append(sorted_offer_keys[r])
                        identical_keys_indices.append(r)
                        
                r+=1
            if len(identical_keys) > 1:
                shuffle(identical_keys)
                s = 0
                for k in identical_keys_indices:
                    sorted_offer_keys[k] = identical_keys[s]
                    s+=1
            q+=1
            
        if model.print_stuff == True:
            print('Electricity offer keys (resorted):',sorted_offer_keys)
        
        #initialize the amounts purchased and produced to zero
        #initialized the remaing supply of suppliers to their offer quantities
        for i in sorted_request_keys:
            self.electricity_purchased_dictionary[i] = 0
        for j in sorted_offer_keys:
            self.electricity_produced_dictionary[j] = 0
            self.remaining_electricity_supply_dictionary[j] = self.electricity_offer_quantity_dictionary[j]
        
        #initialize some variables
        clearing_price_flag = False
        current_request_price = -1
        current_offer_price = -1
        previous_request_price = -1
        previous_offer_price = -1
        suppliers_to_delete = []
        
        #iterate through the demand agents
        for i in sorted_request_keys:
            
            if model.print_stuff == True:
                print(' ')
                print('agent', i, 'buying electricity')
            
            current_request_price = self.electricity_request_price_dictionary[i]
            remaining_demand = self.electricity_request_quantity_dictionary[i]
            
            if model.print_stuff == True:
                print('remaining demand of agent', i, ':', remaining_demand)
            
            #if there's still demand and the market hasn't been cleared
            if remaining_demand > 0 and clearing_price_flag == False:
                
                #iterate through the supply agents
                for j in sorted_offer_keys:
                    
                    current_offer_price = self.electricity_offer_price_dictionary[j]
                    remaining_supply = self.remaining_electricity_supply_dictionary[j]
                    
                    #if there's still demand and the market hasn't been cleared
                    if remaining_demand > 0 and clearing_price_flag == False:
                        
                        if model.print_stuff == True:
                            print('agent', j, 'is selling to agent', i)
                            print('remaining supply of agent', j, ':', remaining_supply)
                            print('request price is', current_request_price)
                            print('offer price is', current_offer_price)
                        
                        #if the request price is still greater than the offer price, we can make a match                        
                        if current_request_price >= current_offer_price:
                            
                            if model.print_stuff == True:
                                print('request price >= offer price, we can make a match')
                        
                            #determine the quantity to be traded between these two agents                            
                            if remaining_demand >= remaining_supply:
                                quantity_to_trade = remaining_supply
                                remaining_demand = remaining_demand - quantity_to_trade
                                remaining_supply = 0
                            else:
                                quantity_to_trade = remaining_demand
                                remaining_demand = 0
                                remaining_supply = remaining_supply - quantity_to_trade
                            
                            self.electricity_purchased_dictionary[i] = self.electricity_purchased_dictionary[i] + quantity_to_trade 
                            self.electricity_produced_dictionary[j] = self.electricity_produced_dictionary[j] + quantity_to_trade
                            self.remaining_electricity_supply_dictionary[j] = remaining_supply
                            
                            #we will remove suppliers from the list if they don't have any remaining supply available
                            if remaining_supply == 0:
                                suppliers_to_delete.append(sorted_offer_keys.index(j))
                            
                            previous_request_price = current_request_price
                            previous_offer_price = current_offer_price
                        
                        #no more matches possible; set the clearing price
                        else:                  
                            self.electricity_clearing_price = (previous_request_price + previous_offer_price) / 2
                            clearing_price_flag = True
                            if model.print_stuff == True:
                                print('CLEARING PRICE FOUND')
            
                #delete the suppliers from the list who don't have any remaining supply available.
                for supplier in reversed(suppliers_to_delete):   
                    del sorted_offer_keys[supplier]
                suppliers_to_delete = []
                
        if model.print_stuff == True:
            print(' ')
            print("ELECTRICITY MARKET RESULTS:")
            print('Offer quantities:',self.electricity_offer_quantity_dictionary)
            print('Offer prices:',self.electricity_offer_price_dictionary) 
            print('Request quantities:',self.electricity_request_quantity_dictionary)
            print('Request prices:',self.electricity_request_price_dictionary)
            print('Electricity produced:',self.electricity_produced_dictionary)   
            print('Electricity purchased:',self.electricity_purchased_dictionary) 
            print('Clearing price:',self.electricity_clearing_price)
    
    
    def clear_heat_market(self, model):
        if model.print_stuff == True:
            print(' ')
            print("MARKET AGENT CLEARING THE HEAT MARKET")
        
        #get a list of the demand agent ids sorted by decreasing request price
        sorted_request_prices = sorted(self.heat_request_price_dictionary.items(), key=operator.itemgetter(1), reverse=True)
        sorted_request_keys = []
        sorted_request_prices_list = []
        for i in sorted_request_prices:
            sorted_request_keys.append(i[0])
            sorted_request_prices_list.append(i[1])
        if model.print_stuff == True:
            print('purchasing agents:', sorted_request_keys)
            
        #randomize the order of the agents with identical request prices
        #go through the list of request prices. if you find multiple identical values in a row
        #randomize the order of the corresponding keys with that value
        if model.print_stuff == True:
            print(' ')    
            print('Heat request prices:',sorted_request_prices_list)
            print('Heat request keys:',sorted_request_keys)
        
        q = 0
        for i in sorted_request_prices_list:
            identical_keys = []
            identical_keys_indices = []
            identical_keys.append(sorted_request_keys[q])
            identical_keys_indices.append(q)
            
            r = 0
            for j in sorted_request_prices_list:
                if i == j:
                    if sorted_request_keys[q] != sorted_request_keys[r]: 
                        identical_keys.append(sorted_request_keys[r])
                        identical_keys_indices.append(r)
                        
                r+=1
            if len(identical_keys) > 1:
                shuffle(identical_keys)
                s = 0
                for k in identical_keys_indices:
                    sorted_request_keys[k] = identical_keys[s]
                    s+=1
            q+=1
            
        if model.print_stuff == True:
            print('Heat request keys (resorted):',sorted_request_keys)

        #get a list of the supply agent ids sorted by increasing offer price
        sorted_offer_prices = sorted(self.heat_offer_price_dictionary.items(), key=operator.itemgetter(1))
        sorted_offer_keys = []
        sorted_offer_prices_list = []
        for i in sorted_offer_prices:
            sorted_offer_keys.append(i[0])
            sorted_offer_prices_list.append(i[1])
        if model.print_stuff == True:
            print('offering agents:', sorted_offer_keys)
            
        #randomize the order of the agents with identical request prices
        #go through the list of request prices. if you find multiple identical values in a row
        #randomize the order of the corresponding keys with that value
        if model.print_stuff == True:
            print(' ')    
            print('Heat offer prices:',sorted_offer_prices_list)
            print('Heat offer keys:',sorted_offer_keys)            
        
        q = 0
        for i in sorted_offer_prices_list:
            identical_keys = []
            identical_keys_indices = []
            identical_keys.append(sorted_offer_keys[q])
            identical_keys_indices.append(q)
            
            r = 0
            for j in sorted_offer_prices_list:
                if i == j:
                    if sorted_offer_keys[q] != sorted_offer_keys[r]: 
                        identical_keys.append(sorted_offer_keys[r])
                        identical_keys_indices.append(r)
                        
                r+=1
            if len(identical_keys) > 1:
                shuffle(identical_keys)
                s = 0
                for k in identical_keys_indices:
                    sorted_offer_keys[k] = identical_keys[s]
                    s+=1
            q+=1
            
        if model.print_stuff == True:
            print('Heat offer keys (resorted):',sorted_offer_keys)
        
        #initialize the amounts purchased and produced to zero
        #initialized the remaing supply of suppliers to their offer quantities
        for i in sorted_request_keys:
            self.heat_purchased_dictionary[i] = 0
        for j in sorted_offer_keys:
            self.heat_produced_dictionary[j] = 0
            self.remaining_heat_supply_dictionary[j] = self.heat_offer_quantity_dictionary[j]
        
        #initialize some variables
        clearing_price_flag = False
        current_request_price = -1
        current_offer_price = -1
        previous_request_price = -1
        previous_offer_price = -1
        suppliers_to_delete = []
        
        #iterate through the demand agents
        for i in sorted_request_keys:
            
            if model.print_stuff == True:
                print(' ')
                print('agent', i, 'buying heat')
           
            current_request_price = self.heat_request_price_dictionary[i]
            remaining_demand = self.heat_request_quantity_dictionary[i]
            
            if model.print_stuff == True:
                print('remaining demand of agent', i, ':', remaining_demand)
            
            #if there's still demand and the market hasn't been cleared
            if remaining_demand > 0 and clearing_price_flag == False:
                
                #iterate through the supply agents
                for j in sorted_offer_keys:
                    current_offer_price = self.heat_offer_price_dictionary[j]
                    remaining_supply = self.remaining_heat_supply_dictionary[j]
                    
                    #if there's still demand and the market hasn't been cleared
                    if remaining_demand > 0 and clearing_price_flag == False:
                        
                        if model.print_stuff == True:
                            print('agent', j, 'is selling to agent', i)
                            print('remaining supply of agent', j, ':', remaining_supply)
                            print('request price is', current_request_price)
                            print('offer price is', current_offer_price)
                        
                        #if the request price is still greater than the offer price, we can make a match                        
                        if current_request_price >= current_offer_price:
                            
                            if model.print_stuff == True:
                                print('request price >= offer price, we can make a match')
                        
                            #determine the quantity to be traded between these two agents                            
                            if remaining_demand >= remaining_supply:
                                quantity_to_trade = remaining_supply
                                remaining_demand = remaining_demand - quantity_to_trade
                                remaining_supply = 0
                            else:
                                quantity_to_trade = remaining_demand
                                remaining_demand = 0
                                remaining_supply = remaining_supply - quantity_to_trade
                            
                            self.heat_purchased_dictionary[i] = self.heat_purchased_dictionary[i] + quantity_to_trade 
                            self.heat_produced_dictionary[j] = self.heat_produced_dictionary[j] + quantity_to_trade
                            self.remaining_heat_supply_dictionary[j] = remaining_supply
                            
                            #we will remove suppliers from the list if they don't have any remaining supply available
                            if remaining_supply == 0:
                                suppliers_to_delete.append(sorted_offer_keys.index(j))
                            
                            previous_request_price = current_request_price
                            previous_offer_price = current_offer_price
                        
                        #no more matches possible; set the clearing price
                        else:                  
                            self.heat_clearing_price = (previous_request_price + previous_offer_price) / 2
                            clearing_price_flag = True
                            if model.print_stuff == True:
                                print('CLEARING PRICE FOUND')
            
                #delete the suppliers from the list who don't have any remaining supply available.
                for supplier in reversed(suppliers_to_delete):                
                    del sorted_offer_keys[supplier]
                suppliers_to_delete = []
                
        if clearing_price_flag == False:
            self.heat_clearing_price = (previous_request_price + previous_offer_price) / 2
            if model.print_stuff == True:
                print('EXCESS SUPPLY. CLEARING PRICE SET')
                
        if model.print_stuff == True:
            print(' ')
            print("HEAT MARKET RESULTS:")
            print('Offer quantities:',self.heat_offer_quantity_dictionary)
            print('Offer prices:',self.heat_offer_price_dictionary) 
            print('Request quantities:',self.heat_request_quantity_dictionary)
            print('Request prices:',self.heat_request_price_dictionary)
            print('Heat produced:',self.heat_produced_dictionary)   
            print('Heat purchased:',self.heat_purchased_dictionary) 
            print('Clearing price:',self.heat_clearing_price)
           
           
    def update_results(self, model):
        
        #append the results to the clearing price list for data collection purposes
        self.electricity_clearing_price_list.append(self.electricity_clearing_price)
        self.heat_clearing_price_list.append(self.heat_clearing_price)
        
    def clear_results(self, model):
        
        #clear the clearing price lists
        self.electricity_clearing_price_list = []
        self.heat_clearing_price_list = []

    def clear_market_results(self, model):
        
        #reset the market variables
        self.electricity_offer_quantity_dictionary = {}
        self.electricity_offer_price_dictionary = {}
        self.electricity_request_quantity_dictionary = {}
        self.electricity_request_price_dictionary = {}
        self.electricity_produced_dictionary = {}
        self.electricity_purchased_dictionary = {}
        self.remaining_electricity_supply_dictionary = {}
        self.electricity_clearing_price = None
        self.total_electricity_demand = 0
        
        self.heat_offer_quantity_dictionary = {}
        self.heat_offer_price_dictionary = {}
        self.heat_request_quantity_dictionary = {}
        self.heat_request_price_dictionary = {}
        self.heat_produced_dictionary = {}
        self.heat_purchased_dictionary = {}
        self.remaining_heat_supply_dictionary = {}
        self.heat_clearing_price = None
        self.total_heat_demand = 0
    
    def step(self, model):
        
        #clear the results from the previous step
        for agent in model.schedule.agents:
            agent.clear_results(model)     
            
            if agent.type == 'energy storage technology':
                agent.reset_charge_state()
                    
            if agent.type == 'energy storage technology' or agent.type == 'energy conversion technology':
                agent.reset_assets()
                
        
        #iterate through the different increments
        for current_increment in range(model.number_of_increments):
            
            #set the current increment
            model.set_current_increment(current_increment)
            
            if model.print_stuff == True:            
                print(' ')
                print('*** INCREMENT', model.current_increment, '***')
            
            #clear the market results from the last increment
            self.clear_market_results(model)
            
            #set some dynamic variables for each agent, and reset the offer and request tuples
            for agent in model.schedule.agents:
                if hasattr(agent, 'supply_agent'):
                    agent.set_heat_capacity(model)
                    agent.offer_tuple = None
                if hasattr(agent, 'demand_agent'):
                    agent.set_heat_demand(model)
                    agent.request_tuple = None
            
            #clear the heat market
            self.aggregate_heat_requests(model)
            self.aggregate_heat_offers(model)
            self.clear_heat_market(model)
            
            for agent in model.schedule.agents:
                if hasattr(agent, 'technology'):
                    agent.update_heat_purchased(self)
                    agent.update_heat_produced(self)

            #set some dynamic variables for each agent
            for agent in model.schedule.agents:
                if hasattr(agent, 'supply_agent'):
                    agent.set_electricity_capacity(model)
                if hasattr(agent, 'demand_agent'):
                    agent.set_electricity_demand(model)            
            
            #clear the electricity market
            self.aggregate_electricity_requests(model)
            self.aggregate_electricity_offers(model)
            self.clear_electricity_market(model)
            for agent in model.schedule.agents:
                if hasattr(agent, 'technology'):
                    agent.update_electricity_purchased(self)
                    agent.update_electricity_produced(self)
            
            #agents update assets and strategy based on the market results
            for agent in model.schedule.agents:
                if hasattr(agent, 'technology'):
                    if model.print_stuff == True:
                        print(' ')
                        print('AGENT PROCESSING MARKET RESULTS:', agent.technology_name, agent.unique_id)
                    if agent.type != 'grid' and agent.type != 'building':
                        if model.print_stuff == True:
                            print('Updating marginal costs')
                        agent.set_marginal_costs(self)
                        
                        if model.print_stuff == True:
                            print('Updating assets')
                        agent.update_assets(self, model)
                        
                        if agent.type == 'energy storage technology':
                            
#                            print(' ')
#                            print('STEP:', model.schedule.steps, 'INCREMENT:', model.current_increment)
#                            print('Electricity purchased by battery:', agent.electricity_purchased, 'at', agent.electricity_request_price)
#                            print('Electricity produced by battery:', agent.electricity_produced, 'at', agent.electricity_offer_price)
#                            print('Reward of battery:', agent.reward)
                            
                            agent.set_charge_state()
                            if model.print_stuff == True:
                                print('Updating charge state. New charge state of', agent.technology_name, agent.unique_id, 'is', agent.current_charge)
                            
                        #if model.schedule.steps <= model.learning_period - 1:
                        if model.print_stuff == True:
                            print('Updating strategy')
                        agent.update_strategy(model)
               
                #update agent results lists with the new data  
                agent.update_results(model)