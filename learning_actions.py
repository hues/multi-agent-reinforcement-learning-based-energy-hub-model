# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 13:36:03 2015

@author: boa
"""
from qlearn import QLearn

class QLearnAgent(object):
    
    def __init__(self, model):
        
        self.learning_agent = True
        
        #initialize some learning agent variables
        self.last_state = None
        self.current_state = None
        self.last_action = None
        self.reward = 0
        self.q_values=[] #used for record-keeping

        #create a dictionary of all possible actions
        self.action_dictionary = QLearnAgent.create_action_dictionary(self, model)
        
        #initialize the learning method
        if self.type == 'energy conversion technology':
            self.learning_method = QLearn(actions=list(self.action_dictionary.keys()),epsilon=model.epsilon, alpha=model.alpha, gamma=model.gamma) 
        elif self.type == 'energy storage technology':
            self.learning_method = QLearn(actions=list(self.action_dictionary.keys()),epsilon=model.epsilon_storage, alpha=model.alpha_storage, gamma=model.gamma_storage)
        
        
        #initialize all q-values to zero        
        QLearnAgent.initialize_learning(self, model)        
        
    def create_action_dictionary(self, model):
        
        #initialize offer lists
        eprice_offer_values = [0]
        equantity_offer_values = [0]
        hprice_offer_values = [0]
        hquantity_offer_values = [0]        
            
        #populate the offer lists
        if hasattr(self, 'supply_agent') or hasattr(self, 'demand_agent'):
            eprice_offer_values, equantity_offer_values, hprice_offer_values, hquantity_offer_values = QLearnAgent.generate_offer_values(self, model)
        
        #fill the action dictionary with the values in the created lists
        action_dictionary = {}
        i = 0
        for equantity_offer in equantity_offer_values:
            for eprice_offer in eprice_offer_values:
                for hquantity_offer in hquantity_offer_values:
                    for hprice_offer in hprice_offer_values:
                        action_dictionary[i] = (equantity_offer, eprice_offer, hquantity_offer, hprice_offer)          
                        i+=1
        
        if model.print_stuff == True:
            print('ACTION DICTIONARY OF AGENT', self.technology_name, self.unique_id, 'IS LENGTH', i)
            print('ELECTRICITY OFFER RANGE: Min:', min(equantity_offer_values), 'Max:', max(equantity_offer_values))
            print('HEAT OFFER RANGE: Min:', min(hquantity_offer_values), 'Max:', max(hquantity_offer_values))
            #print('Action dictionary of agent', self.technology_name, self.unique_id, ':', action_dictionary)
            
        return action_dictionary

            
    def generate_offer_values(self, model):
        
        #set the bounds of offer prices and quantities
        #min_electricity_offer_price = self.marginal_cost_electricity
        #min_heat_offer_price = self.marginal_cost_heat
        min_electricity_offer_price = 0
        min_heat_offer_price = 0
        max_electricity_offer_price = max(model.grid_electricity_price)
        max_heat_offer_price = max(model.grid_electricity_price)
        
        if self.capacity_electricity > 0 and self.type == 'energy storage technology':
            min_electricity_offer_quantity = -100 #a negative offer quantity constitutes a request
        else:
            min_electricity_offer_quantity = 0
            
        if self.capacity_heat > 0 and self.type == 'energy storage technology':
            min_heat_offer_quantity = -100 #a negative offer quantity constitutes a request
        else:
            min_heat_offer_quantity = 0
        
        if self.capacity_electricity > 0:
            max_electricity_offer_quantity = 100 #we express this as a percentage of the current capacity
        else:
            max_electricity_offer_quantity = 0
        
        if self.capacity_heat > 0:
            max_heat_offer_quantity = 100 #we express this as a percentage of the current capacity
        else:
            max_heat_offer_quantity = 0
        
        #determine the increments
        if self.type == 'energy storage technology':
            price_increment = model.learning_price_increment_storage
            quantity_increment = model.learning_quantity_increment_storage
            #quantity_increment = 100
        else:
            price_increment = model.learning_price_increment
            quantity_increment = model.learning_quantity_increment
        
        #create the offer price and quantity lists
        if max_electricity_offer_price >= min_electricity_offer_price and max_electricity_offer_quantity >= min_electricity_offer_quantity and max_electricity_offer_quantity > 0:
            eprice_offer_values = list(QLearnAgent.frange(min_electricity_offer_price, 
                                                          max_electricity_offer_price + price_increment, price_increment))
            equantity_offer_values = list(QLearnAgent.frange(min_electricity_offer_quantity, max_electricity_offer_quantity + quantity_increment, 
                                                             quantity_increment))
        else:
            eprice_offer_values = [0]
            equantity_offer_values = [0]
        
        if max_heat_offer_price >= min_heat_offer_price and max_heat_offer_quantity >= min_heat_offer_quantity and max_heat_offer_quantity > 0:
            hprice_offer_values = list(QLearnAgent.frange(min_heat_offer_price, max_heat_offer_price + price_increment, price_increment))
            hquantity_offer_values = list(QLearnAgent.frange(min_heat_offer_quantity, max_heat_offer_quantity + quantity_increment, 
                                                             quantity_increment))   
        else:
            hprice_offer_values = [0]
            hquantity_offer_values = [0]
        
        #IGNORE THIS FOR NOW
        #remove elements violating minimum load constraints 
#        if self.minimum_load > 0:
#            i = 0
#            indices_to_remove = []
#            for value in equantity_offer_values:
#                if value > 0 and value < self.minimum_load:
#                    indices_to_remove.append(i)
#                i+=1
#            del equantity_offer_values[indices_to_remove]
#                
#            i = 0
#            indices_to_remove = []
#            for value in hquantity_offer_values:
#                if value > 0 and value < self.minimum_load:
#                    indices_to_remove.append(i)
#                i+=1
#            del hquantity_offer_values[indices_to_remove]
        
                #the electricity produced by the CHP is determined by the heat produced, so we don't have to include this
        
        #energy conversion technologies only offer their full capacity
        if self.type == 'energy conversion technology':
            equantity_offer_values = [0]
            hquantity_offer_values = [0]
        
        #CHP electricity offer values are determined by the heat market 
        if self.technology_name == 'CHP':
            equantity_offer_values = [0]
            eprice_offer_values = [0]
        
        if model.print_stuff == True:
            print('ELECTRICITY OFFERS: Quantity range:', min(equantity_offer_values), ':', max(equantity_offer_values), '; Price range:', min(eprice_offer_values), ':', max(eprice_offer_values))
            print('HEAT OFFERS: Quantity range:', min(hquantity_offer_values), ':', max(hquantity_offer_values), '; Price range:', min(hprice_offer_values), ':', max(hprice_offer_values))
        
        #return the quantity and price lists
        return eprice_offer_values, equantity_offer_values, hprice_offer_values, hquantity_offer_values
    
    #helper function like range, but capable of handling floats
    @staticmethod    
    def frange(start, stop, step):
        i = start
        while i < stop:
            yield i
            i += step

    def initialize_learning(self, model):

        if model.print_stuff == True:
            print(self.technology_name, self.unique_id, 'initalizing Q-values')
        
        #initialize the Q-values for the energy conversion technologies   
        i = 0
        if self.type == 'energy conversion technology':
            for increment in range(model.number_of_increments):
                state = (increment,100) #the only variable state value for energy conversion technologies is the increment
                QLearnAgent.initializeQ(self, state)
                i += 1
                
        elif self.type == 'energy storage technology':
            for increment in range(model.number_of_increments):
                for charge in range(0, 100 + model.learning_quantity_increment_storage, model.learning_quantity_increment_storage):
                    state = (increment, charge) #the state for storage technologies is composed of increment and charge
                    QLearnAgent.initializeQ(self, state)
                    i += 1
        
        if model.print_stuff == True:
            print('NUMBER OF STATES', self.technology_name, self.unique_id, ':', i)
                
    def initializeQ(self, state):
        
        #initialize the q-values to zero
        for action in self.learning_method.actions:
            self.learning_method.q[(state,action)] = 0.0
    
    def query_action_dictionary(self, model):
        
        if model.print_stuff == True:
            print(self.technology_name, self.unique_id, 'querying action dictionary')        
        
        #set the current state
        if self.type =='energy storage technology':       
            charge_fraction = 100 * self.current_charge / self.storage_capacity
            rounded_charge_fraction = round(charge_fraction / model.learning_quantity_increment_storage) * model.learning_quantity_increment_storage

            self.current_state = (model.current_increment, rounded_charge_fraction)
       
        elif self.type == 'energy conversion technology':
            self.current_state = (model.current_increment,100)
        
        epsilon_factor = model.epsilon - model.epsilon * (model.schedule.steps + 1) / model.learning_period
        #select an action based on the learning results
        #offer_key = self.learning_method.chooseActionBoltzmann(self.current_state,model.boltzmann_temperature)
        offer_key = self.learning_method.chooseActionEpsilonFactor(self.current_state, epsilon_factor)
        #offer_key = self.learning_method.chooseAction(self.current_state)
        if model.schedule.steps >= model.learning_period:
            offer_key = self.learning_method.chooseActionEpsilonFactor(self.current_state, 0)
        
        #set the selected action as the last action
        self.last_action = offer_key
        
        #extract the offer tuple from the action dictionary
        offer_tuple = self.action_dictionary.get(offer_key)
        
        return offer_tuple
    
    def set_reward(self, model):
        
        self.reward = self.profit
    
    def update_action_dictionary(self, model):
        
        #set the last state as the current state        
        self.last_state = self.current_state
        
        #set the current state as the next state        
        if self.type =='energy storage technology':
            charge_fraction = 100 * self.current_charge / self.storage_capacity
            rounded_charge_fraction = round(charge_fraction / model.learning_quantity_increment_storage) * model.learning_quantity_increment_storage
            if model.current_increment == model.number_of_increments - 1:
                self.current_state = (0, 100 * self.initial_charge / self.storage_capacity)
            else:
                self.current_state = (model.current_increment + 1, rounded_charge_fraction)
        
        elif self.type == 'energy conversion technology':
            self.current_state = ((model.current_increment + 1) % model.number_of_increments, 100)
        
        if model.print_stuff == True:
            print('last state =', self.last_state,', last action =',self.last_action,', reward =',self.reward,', current state =',self.current_state)
        
#        if self.type =='energy storage technology':
#            print('Old Q: State:', self.last_state, 'Action:', self.action_dictionary.get(self.last_action), 'Q-value:', self.learning_method.getQ(self.last_state, self.last_action))
        
        #update the q-values  
        if model.current_increment == model.number_of_increments - 1:
            self.learning_method.learn_last_increment(self.last_state, self.last_action, self.reward)
        else:
            self.learning_method.learn(self.last_state, self.last_action, self.reward, self.current_state)
            
        
        #if model.schedule.steps > model.learning_period - 100 and self.type =='energy storage technology' and self.learning_method.getQ(self.last_state, self.last_action) > 1.5:
#        if self.type =='energy storage technology':
#            print('New Q: State:', self.last_state, 'Action:', self.action_dictionary.get(self.last_action), 'Q-value:', self.learning_method.getQ(self.last_state, self.last_action))
        
        #extract the q-values for record-keeping purposes        
        self.q_values = list(self.learning_method.q.values())