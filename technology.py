# -*- coding: utf-8 -*-
"""
Created on Wed Oct 28 14:55:50 2015

@author: boa
"""
from learning_actions import QLearnAgent
from learning_actions_cacla import CaclaAgent

class Technology(object):
    
    def __init__(self, model):
        
        self.technology = True
        
        #initialize the basic technology variables
        self.electricity_request_price = -1
        self.electricity_request_quantity = -1
        self.electricity_purchased = -1
        self.heat_request_price = -1
        self.heat_request_quantity = -1
        self.heat_purchased = -1
        self.electricity_request_price_list = []
        self.electricity_request_quantity_list = []
        self.electricity_purchased_list = []
        self.heat_request_price_list = []
        self.heat_request_quantity_list = []
        self.heat_purchased_list = []
        
        self.electricity_offer_price = -1
        self.electricity_offer_quantity = -1
        self.electricity_produced = -1
        self.heat_offer_price = -1
        self.heat_offer_quantity = -1
        self.heat_produced = -1
        self.electricity_offer_price_list = []
        self.electricity_offer_quantity_list = []
        self.electricity_produced_list = []
        self.heat_offer_price_list = []
        self.heat_offer_quantity_list = []
        self.heat_produced_list = []
        
        self.assets = 0
        self.profit = 0        
        self.profit_list = []
        
        self.tuple1 = -1
        self.tuple2 = -1
        self.tuple3 = -1
        self.tuple4 = -1
        
        self.tuple1_list = []
        self.tuple2_list = []
        self.tuple3_list = []
        self.tuple4_list = []
        
    def update_electricity_purchased(self, market):  
        
        #set the electricity purchased according to the market results
        if hasattr(self, 'demand_agent') and self.electricity_request_quantity > 0:
            self.electricity_purchased = market.electricity_purchased_dictionary[self.unique_id]
        else:
            self.electricity_purchased = 0
        
        #if self.technology_name == 'battery':
            #print('purchased:', self.electricity_purchased)
        
    def update_heat_purchased(self, market):
        
        #set the heat purchased according to the market results
        if hasattr(self, 'demand_agent') and self.heat_request_quantity > 0:
            self.heat_purchased = market.heat_purchased_dictionary[self.unique_id]
        else:
            self.heat_purchased = 0
        
    def update_electricity_produced(self, market):
        
        #set the electricity produced according to the market results
        if hasattr(self, 'supply_agent') and self.electricity_offer_quantity > 0:
            self.electricity_produced = market.electricity_produced_dictionary[self.unique_id]
        else:
            self.electricity_produced = 0
        
        #if self.technology_name == 'battery':
            #print('produced:', self.electricity_produced)
        
    def update_heat_produced(self, market):
        
        #set the heat produced according to the market results
        if hasattr(self, 'supply_agent') and self.heat_offer_quantity > 0:
            self.heat_produced = market.heat_produced_dictionary[self.unique_id]
        else:
            self.heat_produced = 0

    def update_assets(self, market, model):
        
        #update assets and reward according to the market results
        income = self.electricity_produced * self.electricity_offer_price + self.heat_produced * self.heat_offer_price
        #expenditures = self.electricity_produced * self.marginal_cost_electricity + self.heat_produced * self.marginal_cost_heat + self.fixed_cost_per_hour
        expenditures = self.electricity_produced * self.marginal_cost_electricity + self.heat_produced * self.marginal_cost_heat
        
        #for CHPs, the electricity is produced automatically when heat is produced, and the resulting costs are incurred
        if self.technology_name == 'CHP':
            #expenditures = self.heat_produced / self.heat_to_power_ratio * self.marginal_cost_electricity + self.heat_produced * self.marginal_cost_heat + self.fixed_cost_per_hour
            expenditures = (self.heat_produced / self.heat_to_power_ratio) * self.marginal_cost_electricity + self.heat_produced * self.marginal_cost_heat
            
        if self.technology_name == 'battery':
            expenditures = self.electricity_purchased * self.electricity_request_price
            
        if self.technology_name == 'hot water tank':
            expenditures = self.heat_purchased * self.heat_request_price
        
        self.profit = income - expenditures
        self.assets += self.profit  
        
        #if self.technology_name == 'battery':
            #print('profit:', self.profit)
        
        #if this is a learning agent, set the reward
        if self.type == "energy conversion technology" or self.type == "energy storage technology":
            if model.learning_algorithm == 1:
                QLearnAgent.set_reward(self, model)
            else:
                CaclaAgent.set_reward(self, model)
        #self.reward = (profit / (model.electricity_demand_per_building_agent[model.current_increment] * model.number_of_buildings * model.max_electricity_offer_price + model.heat_demand_per_building_agent[model.current_increment] * model.number_of_buildings * model.max_heat_offer_price))
         
        if model.print_stuff == True:
            print(self.technology_name,self.unique_id," updating assets: profit=",self.profit,", assets=",self.assets,sep="")

    def update_strategy(self, model):  
        
        #update strategy according to the market results
        if model.learning_algorithm == 1:
            QLearnAgent.update_action_dictionary(self, model)
        else:
            CaclaAgent.update_action_dictionary(self, model)
            
    def update_results(self, model):
        
        #update lists for record keeping
        self.electricity_request_price_list.append(self.electricity_request_price)
        self.electricity_request_quantity_list.append(self.electricity_request_quantity)
        self.electricity_purchased_list.append(self.electricity_purchased)
        self.heat_request_price_list.append(self.heat_request_price)
        self.heat_request_quantity_list.append(self.heat_request_quantity)
        self.heat_purchased_list.append(self.heat_purchased)
        
        self.electricity_offer_price_list.append(self.electricity_offer_price)
        self.electricity_offer_quantity_list.append(self.electricity_offer_quantity)
        self.electricity_produced_list.append(self.electricity_produced)
        self.heat_offer_price_list.append(self.heat_offer_price)
        self.heat_offer_quantity_list.append(self.heat_offer_quantity)
        self.heat_produced_list.append(self.heat_produced)
        
        self.profit_list.append(self.profit)
        
        self.tuple1_list.append(self.tuple1)
        self.tuple2_list.append(self.tuple2)
        self.tuple3_list.append(self.tuple3)
        self.tuple4_list.append(self.tuple4)
        
    def clear_results(self, model):
        
        #clear the lists (happens at the end of each step)
        self.electricity_request_price_list = []
        self.electricity_request_quantity_list = []
        self.electricity_purchased_list = []
        self.heat_request_price_list = []
        self.heat_request_quantity_list = []
        self.heat_purchased_list = []
        
        self.electricity_offer_price_list = []
        self.electricity_offer_quantity_list = []
        self.electricity_produced_list = []
        self.heat_offer_price_list = []
        self.heat_offer_quantity_list = []
        self.heat_produced_list = []
            
        self.profit_list = []
        
        self.tuple1_list = []
        self.tuple2_list = []
        self.tuple3_list = []
        self.tuple4_list = []
        
    def step(self, model):
        pass