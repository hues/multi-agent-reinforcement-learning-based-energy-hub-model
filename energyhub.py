# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 09:48:30 2015

@author: boa
"""

from mesa import Model
from mesa.time import RandomActivation
from mesa.datacollection import DataCollector
import numpy
import energy_conversion_technologies
import energy_storage_technologies
import buildings
import markets
import grid

class EnergyHubModel(Model):
    def __init__(self, learning_algorithm, number_of_steps, learning_period, number_of_increments, number_of_heat_pumps, number_of_gas_boilers, 
                       number_of_CHP_units, number_of_solar_PV_units, number_of_solar_thermal_units, number_of_buildings, 
                       number_of_batteries, number_of_heat_tanks,
                       grid_electricity_price, grid_electricity_offer_price, natural_gas_price, interest_rate, solar_insolation,
                       electricity_demand_per_building_agent, heat_demand_per_building_agent,
                       epsilon, alpha, gamma, epsilon_storage, alpha_storage, gamma_storage, boltzmann_temperature,
                       learning_price_increment, learning_quantity_increment,
                       learning_price_increment_storage, learning_quantity_increment_storage,
                       actor_layout, replication_number, alpha_cacla, beta_cacla, gamma_cacla, sigma_cacla, folder_cacla,
                       actor_layout_storage, alpha_cacla_storage, beta_cacla_storage, gamma_cacla_storage, sigma_cacla_storage,
                       transfer_functions, transfer_functions_storage, print_stuff):
        
        #initialize the model variables
        self.learning_algorithm = learning_algorithm        
        self.number_of_steps = number_of_steps
        self.learning_period = learning_period
        self.number_of_increments = number_of_increments
        self.number_of_heat_pumps = number_of_heat_pumps
        self.number_of_gas_boilers = number_of_gas_boilers
        self.number_of_CHP_units = number_of_CHP_units
        self.number_of_solar_PV_units = number_of_solar_PV_units
        self.number_of_solar_thermal_units = number_of_solar_thermal_units
        self.number_of_buildings = number_of_buildings
        self.number_of_batteries = number_of_batteries
        self.number_of_heat_tanks = number_of_heat_tanks
        self.grid_electricity_price = grid_electricity_price 
        self.grid_electricity_offer_price = grid_electricity_offer_price 
        self.natural_gas_price = natural_gas_price
        self.interest_rate = interest_rate
        self.solar_insolation = solar_insolation
        self.electricity_demand_per_building_agent = electricity_demand_per_building_agent
        self.heat_demand_per_building_agent = heat_demand_per_building_agent
        self.epsilon = epsilon
        self.alpha = alpha
        self.gamma = gamma 
        self.epsilon_storage = epsilon_storage
        self.alpha_storage = alpha_storage
        self.gamma_storage = gamma_storage  
        self.boltzmann_temperature = boltzmann_temperature
        self.learning_price_increment = learning_price_increment
        self.learning_quantity_increment = learning_quantity_increment
        self.learning_price_increment_storage  = learning_price_increment_storage
        self.learning_quantity_increment_storage = learning_quantity_increment_storage
        self.actor_layout = actor_layout
        self.replication_number = replication_number
        self.alpha_cacla = alpha_cacla
        self.beta_cacla = beta_cacla
        self.gamma_cacla = gamma_cacla
        self.sigma_cacla = sigma_cacla
        self.actor_layout_storage = actor_layout_storage
        self.alpha_cacla_storage = alpha_cacla_storage
        self.beta_cacla_storage = beta_cacla_storage
        self.gamma_cacla_storage = gamma_cacla_storage
        self.sigma_cacla_storage = sigma_cacla_storage
        self.transfer_functions = transfer_functions
        self.transfer_functions_storage = transfer_functions_storage 
        self.folder_cacla = folder_cacla
        self.print_stuff = print_stuff
        
        self.current_increment = 0
        self.schedule = RandomActivation(self)
        
        #create the agents        
        self.create_agents()
        
        #calculate the maximum reward
        self.max_reward = self.calculate_max_reward()
        
        #set the variables to return
        model_reporters = {
            "mean_electricity_offer_price": lambda m: self.calculate_mean_offer_values(m)[0],
            "mean_electricity_offer_quantity": lambda m: self.calculate_mean_offer_values(m)[1],
            "mean_heat_offer_price": lambda m: self.calculate_mean_offer_values(m)[2],
            "mean_heat_offer_quantity": lambda m: self.calculate_mean_offer_values(m)[3],
            "current_increment": lambda m: self.current_increment
        }
        agent_reporters = {
            "unique_id": lambda a: getattr(a, "unique_id", None),
            "type": lambda a: getattr(a, "type", None),
            "technology_name": lambda a: getattr(a, "technology_name", None),
            "electricity_offer_quantity_list": lambda a: getattr(a, "electricity_offer_quantity_list", None),
            "electricity_offer_price_list": lambda a: getattr(a, "electricity_offer_price_list", None),
            "electricity_request_quantity_list": lambda a: getattr(a, "electricity_request_quantity_list", None),
            "electricity_request_price_list": lambda a: getattr(a, "electricity_request_price_list", None),
            "electricity_produced_list": lambda a: getattr(a, "electricity_produced_list", None),
            "electricity_purchased_list": lambda a: getattr(a, "electricity_purchased_list", None),
            "heat_offer_quantity_list": lambda a: getattr(a, "heat_offer_quantity_list", None),
            "heat_offer_price_list": lambda a: getattr(a, "heat_offer_price_list", None),
            "heat_request_quantity_list": lambda a: getattr(a, "heat_offer_quantity_list", None),
            "heat_request_price_list": lambda a: getattr(a, "heat_offer_price_list", None),
            "heat_produced_list": lambda a: getattr(a, "heat_produced_list", None),
            "heat_purchased_list": lambda a: getattr(a, "heat_purchased_list", None),
            "marginal_cost_electricity": lambda a: getattr(a, "marginal_cost_electricity", None),
            "marginal_cost_heat": lambda a: getattr(a, "marginal_cost_heat", None),
            "electricity_clearing_price_list": lambda a: getattr(a, "electricity_clearing_price_list", None),
            "heat_clearing_price_list": lambda a: getattr(a, "heat_clearing_price_list", None),
            "profit_list": lambda a: getattr(a, "profit_list", None),
            "assets": lambda a: getattr(a, "assets", None),
            "q_values": lambda a: getattr(a, "q_values", None),
            "tuple1_list": lambda a: getattr(a, "tuple1_list", None),
            "tuple2_list": lambda a: getattr(a, "tuple2_list", None),
            "tuple3_list": lambda a: getattr(a, "tuple3_list", None),
            "tuple4_list": lambda a: getattr(a, "tuple4_list", None),
            "current_charge": lambda a: getattr(a, "current_charge", None)
            
        }
        self.data_collector = DataCollector(model_reporters=model_reporters, agent_reporters=agent_reporters)

    def create_agents(self):
        
        agents_created = 1
        
        #create the energy conversion technologies
        for i in range(self.number_of_heat_pumps):
            if self.print_stuff == True:
                print(' ')
                print("CREATING HEAT PUMP", agents_created)
            t = energy_conversion_technologies.HeatPump(self,agents_created)
            self.schedule.add(t)
            agents_created += 1   
        
        for j in range(self.number_of_gas_boilers):
            if self.print_stuff == True:
                print(' ')
                print("CREATING GAS BOILER", agents_created)            
            t = energy_conversion_technologies.GasBoiler(self,agents_created)
            self.schedule.add(t)
            agents_created += 1  
        
        for k in range(self.number_of_CHP_units):
            if self.print_stuff == True:
                print(' ')
                print("CREATING CHP UNIT", agents_created)            
            t = energy_conversion_technologies.CHP(self,agents_created)
            self.schedule.add(t)
            agents_created += 1  
        
        for l in range(self.number_of_solar_PV_units):
            if self.print_stuff == True:
                print(' ')                
                print("CREATING SOLAR PV UNIT", agents_created)            
            t = energy_conversion_technologies.SolarPV(self,agents_created)
            self.schedule.add(t)
            agents_created += 1  
        
        for m in range(self.number_of_solar_thermal_units):
            if self.print_stuff == True:
                print(' ')     
                print("CREATING SOLAR THERMAL UNIT", agents_created)            
            t = energy_conversion_technologies.SolarThermal(self,agents_created)
            self.schedule.add(t)
            agents_created += 1  
        
        #create the energy storage technologies
        for n in range(self.number_of_batteries):
            if self.print_stuff == True:
                print(' ')
                print("CREATING BATTERY", agents_created)            
            t = energy_storage_technologies.Battery(self,agents_created)
            self.schedule.add(t)
            agents_created += 1  
        
        for p in range(self.number_of_heat_tanks):
            if self.print_stuff == True:
                print(' ')
                print("CREATING HOT WATER TANK", agents_created)            
            t = energy_storage_technologies.HotWaterTank(self,agents_created)
            self.schedule.add(t)
            agents_created += 1 
            
        #create the grid
        if self.print_stuff == True:
            print(' ')
            print("CREATING GRID", agents_created)        
        g = grid.ElectricityGrid(self,agents_created)
        self.schedule.add(g)
        agents_created += 1 
        
        #create the buildings
        for q in range(self.number_of_buildings):
            if self.print_stuff == True:
                print(' ')
                print("CREATING BUILDING", agents_created)            
            b = buildings.BuildingAgent(self,agents_created)
            self.schedule.add(b)
            agents_created += 1
        
        #create the market
        if self.print_stuff == True:
            print(' ')
            print("CREATING MARKET", agents_created) 
        m = markets.MarketAgent(agents_created)
        self.schedule.add(m)
        agents_created += 1
        
        number_of_agents = self.count_agents()
        if self.print_stuff == True:
            print(' ')
            print("TOTAL NUMBER OF AGENTS =", number_of_agents)
    
    def set_current_increment(self,current_increment):
        self.current_increment = current_increment
    
    #helper procedure
    def count_agents(self):
        number_of_agents = 0    
        for agent in self.schedule.agents:
            number_of_agents += 1
        return number_of_agents
        
    def calculate_max_reward(self):
        max_reward = 0
        max_reward_current = 0
        for agent in self.schedule.agents:
            if agent.type == "energy conversion technology" or agent.type == "energy storage technology":
                max_reward_current = agent.capacity_electricity * max(self.grid_electricity_price) + agent.capacity_heat * max(self.grid_electricity_price)
                if max_reward_current > max_reward:
                    max_reward = max_reward_current
        return max_reward
        
    
    @staticmethod #a static method means that self is not implicitly passed as the first argument
    def calculate_mean_offer_values(self):
        for agent in self.schedule.agents:
            if agent.type == 'market':
                eprice_list = list(agent.electricity_offer_price_dictionary.values())
                equantity_list = list(agent.electricity_offer_quantity_dictionary.values())
                hprice_list = list(agent.heat_offer_price_dictionary.values())
                hquantity_list = list(agent.heat_offer_quantity_dictionary.values())
        
        return numpy.mean(eprice_list), numpy.mean(equantity_list), numpy.mean(hprice_list), numpy.mean(hquantity_list)    

    #run the model    
    def step(self):
        self.schedule.step()
        self.data_collector.collect(self)