# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 09:59:42 2015

@author: boa
"""

import random
import math
import numpy

#Modified version of code from https://github.com/studywolf/blog/blob/master/RL/Egocentric/qlearn.py
#More info on techniques for balancing exploration and exploitation: http://artint.info/html/ArtInt_266.html
#How to use a Boltzmann "soft max" control policy with variable temperature: http://computing.dcu.ie/~humphrys/Notes/RL/Code/code.q.html
#Explnation of using Boltzmann distribution: http://computing.dcu.ie/~humphrys/PhD/ch2.html

#INTERESTING IDEA:
#"In fact, while I use simple random exploration (no Boltzmann) to learn the Q-values on the small statespaces, 
#when it came to testing the result instead of using a deterministic strict highest Q I used a low temperature Boltzmann. 
#Strict determinism can lead to a creature with brittle behavior. If there are a number of more-or-less equally good 
#actions with very little difference in their Q-values, we should probably rotate around them a little rather than 
#pick the same one every time." (http://computing.dcu.ie/~humphrys/PhD/ch2.html)

class QLearn:
    def __init__(self, actions, epsilon=0.1, alpha=0.2, gamma=0.9):
        
        #initialize the q-learning variables        
        self.q = {}
        self.epsilon = epsilon
        self.alpha = alpha
        self.gamma = gamma
        self.actions = actions
                
    def getQ(self, state, action):
        #return self.q.get((state, action), 0.0)
        return self.q.get((state, action))

    def learn(self, state, action, reward, state2):

        #cconfirm that the state action pair exists
        if (state,action) not in self.q:
            self.q[(state, action)] = reward
            print('missing state/action pair: ', state, action)

        #if it does, set the new q-value for the state-action pair
        else:
            maxqnew = max([self.getQ(state2, a) for a in self.actions])
            value = reward + self.gamma * maxqnew
            self.q[(state, action)] += self.alpha * (value - self.q[(state, action)])
            
    def learn_last_increment(self, state, action, reward):

        #cconfirm that the state action pair exists
        if (state,action) not in self.q:
            self.q[(state, action)] = reward
            print('missing state/action pair: ', state, action)

        #if it does, set the new q-value for the state-action pair
        else:
            value = reward
            self.q[(state, action)] += self.alpha * (value - self.q[(state, action)])

    #epsilon greedy strategy
    def chooseAction(self, state):
        
        #select a random action
        if random.random() < self.epsilon:
            qval = None
            while qval == None:            
                action = random.choice(self.actions)
                qval = self.getQ(state, action)

        #select the option with the max q-value
        else:
            q = [self.getQ(state, a) for a in self.actions]
            maxQ = max(q)
            #maxQ = max(x for x in q if x is not None)
            count = q.count(maxQ)
            if count > 1:
                best = [i for i in range(len(self.actions)) if q[i] == maxQ]
                i = random.choice(best)
            else:
                i = q.index(maxQ)

            action = self.actions[i]
        return action
    
    #epsilon greedy strategy with reduced exploitation probability over time
    def chooseActionEpsilonFactor(self, state, epsilon_factor):
        
        #select a random action
        if random.random() < self.epsilon * epsilon_factor:
            action = random.choice(self.actions)
        
        #select the option with the max q-value
        else:
            q = [self.getQ(state, a) for a in self.actions]
            maxQ = max(q)
            count = q.count(maxQ)
            if count > 1:
                best = [i for i in range(len(self.actions)) if q[i] == maxQ]
                i = random.choice(best)
            else:
                i = q.index(maxQ)

            action = self.actions[i]
        return action
        
    #TODO: this needs to be verified
    def chooseActionBoltzmann(self, state, temperature):

        q = [self.getQ(state, a) for a in self.actions]
        boltzmann_denominator = sum([math.exp(q_a/temperature) for q_a in q])
        boltzmann_numerator = [math.exp(q_a/temperature) for q_a in q]
        probabilities = [boltzmann_numerator_item / boltzmann_denominator for boltzmann_numerator_item in boltzmann_numerator] 
        action = numpy.random.choice(self.actions, replace=True, p=probabilities)
        return action